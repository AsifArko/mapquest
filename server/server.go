package server

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"hoods.com/mapquest/connectiors"
	"hoods.com/mapquest/models"
	"io/ioutil"
	"net/http"
	pb"hoods.com/mapquest/buffers/map"
)

const (
	GEOCODING_URL = "http://www.mapquestapi.com/geocoding/v1/address"
)

type MapServer struct {
	cfg    *models.Config
	client *http.Client
	db     *connectiors.CouchbaseConnection
}

func GetMapServer(cfg *models.Config, conn *connectiors.CouchbaseConnection) (*MapServer, error) {
	return &MapServer{
		cfg:    cfg,
		db:     conn,
		client: &http.Client{},
	}, nil
}

func (s *MapServer) GeoCoding(ctx context.Context , in *pb.GeoCodingRequest) (*pb.GeoCodingResponse , error) {

	query := fmt.Sprintf("%s?key=%s&inFormat=kvp&outFormat=json&location=Banasree+Dhaka&thumbMaps=false", GEOCODING_URL, in.AccessToken)

	//Generating the request
	req, _ := http.NewRequest("GET", query, nil)

	//Sending the request to the Mapquest server and receiving the response
	res, _ := http.DefaultClient.Do(req)
	defer res.Body.Close()

	//Marshalling the response body
	body, err := ioutil.ReadAll(res.Body)
	if err != nil{
		return nil  , errorMessage(err , "Error Marshalling response body .")
	}

	//Unmarshalling the byte in our GeoCodingResponse
	var resp pb.GeoCodingResponse
	err = json.Unmarshal(body, &resp)
	if err != nil{
		return nil , errorMessage(err, "Error in Unmarshalling data")
	}
	return &resp , nil
}

func errorMessage(err error, context string) error {
	msg := fmt.Sprintf("DELIVERY ::  :: %s :: %s", context, err.Error())
	fmt.Println(msg)
	return errors.New(msg)
}