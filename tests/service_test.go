package tests

import (
	"context"
	"fmt"
	"github.com/johnnadratowski/golang-neo4j-bolt-driver/log"
	"testing"
	"hoods.com/mapquest/server"
	pb"hoods.com/mapquest/buffers/map"
)

const (
	AccessToken = "u7Soje9PqR05Sn1RJ5s2JeHvGkmFjX0C"
)


var mapServer *server.MapServer
var err error

func TestMapService(t *testing.T)  {
	testGeoCoding(t)
}

func testGeoCoding(t *testing.T)  {
	var request pb.GeoCodingRequest

	request.AccessToken = AccessToken
	request.Location = "Banani+Dhaka"

	resp , err := mapServer.GeoCoding(context.Background() , &request)
	if err !=nil{
		log.Fatal(err)
	}

	fmt.Println(resp)
}