package models

type Config struct {

	// Server Information
	Environment string
	ServePort   string // Server Listening Port

	// Database Information
	NoSqlUser      string
	NoSqlPassword  string
	DBHost         string
	DBPort         string
	BucketName     string
	BucketPassword string

	// SSL Com config
	SSLCOMStoreId   string
	SSLCOMStorePass string

	// Token Encryption Credential
	PublicKeyPath  string
	PrivateKeyPath string

	// Sentry Credential
	SentryKey    string
	SentrySecret string
	SentryAPI    string
}
