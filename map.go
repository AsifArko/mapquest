package main

import (
	"encoding/json"
	"fmt"
	"github.com/getsentry/raven-go"
	"github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	"github.com/pkg/profile"
	"github.com/urfave/cli"
	"hoods.com/mapquest/server"
	"google.golang.org/grpc"
	pb "hoods.com/mapquest/buffers/map"
	conn "hoods.com/mapquest/connectiors"
	"hoods.com/mapquest/models"
	"net"
	"os"
	"time"
)

var cfg *models.Config

func init() {

	env := getEnv("ENV", "")

	switch env {
	case "development":
		cfg = &models.Config{}
	case "production":

	default:
		cfg = &models.Config{

			Environment: env,
			ServePort:   "8021",

			//DBHost: "45.77.36.127",
			DBHost:         "localhost",
			DBPort:         "8091",
			NoSqlUser:      "Administrator",
			NoSqlPassword:  "nirfapurba",
			BucketName:     "socrates",
			BucketPassword: "",
		}
	}
}

var (
	recoveryFunc grpc_recovery.RecoveryHandlerFunc
)

func main() {

	defer profile.Start().Stop()

	// Get the Current Date During Building
	current_time := time.Now().Local()

	app := cli.NewApp()
	app.Name = "Delivery App Map Service"
	app.Version = fmt.Sprintf("%s-alpha", current_time.Format("06.01.02")) // get server version by date

	//Cli for Server
	app.Commands = []cli.Command{
		{
			Name:  "run",
			Usage: "Run the Service",
			Action: func(c *cli.Context) {
				// Init gRPCS
				fmt.Println(string(fmtConfig(cfg)))
				listen, err := net.Listen("tcp", fmt.Sprintf(":%s", cfg.ServePort))
				if err != nil {
					errorMessage(err, "gRPC :: Init")
				}

				fmt.Println(fmt.Sprintf("Micro Service is Running on Port %s", cfg.ServePort))

				// Init Connection to DB
				nosql, err := conn.GetCouchbaseConnection(cfg)
				if err != nil {
					errorMessage(err, "INIT")
				}

				// Shared options for the logger, with a custom gRPC code to log level function.
				opts := []grpc_recovery.Option{
					grpc_recovery.WithRecoveryHandler(recoveryFunc),
				}

				// Register only UnaryServerChain , Stream not Used at this time
				rpcServer := grpc.NewServer(
					grpc_middleware.WithUnaryServerChain(
						grpc_recovery.UnaryServerInterceptor(opts...),
					),
				)

				nosqlServer, err := server.GetMapServer(cfg, nosql)
				if err != nil {
					errorMessage(err, "Crud :: Init")
				}
				pb.RegisterMappingServiceServer(rpcServer, nosqlServer)
				err = rpcServer.Serve(listen)
				if err != nil {
					errorMessage(err, "gRPC :: Connect")
				}
			},
		},
	}

	app.Run(os.Args)

}

// #todo better exception handling and logging
func errorMessage(err error, context string) {
	msg := fmt.Sprintf("ApiGateway :: %s :: %s", context, err.Error())
	fmt.Println(msg)

	// Send to Sentry
	raven.CaptureError(err, map[string]string{"profile :: grpc-server": msg})
}

func getEnv(key, defaults string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return defaults
}

func fmtConfig(cfg *models.Config) []byte {
	str, err := json.MarshalIndent(cfg, " ", " ")
	if err != nil {
		return nil
	}
	return str
}
