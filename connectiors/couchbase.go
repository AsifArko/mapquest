package connectiors

import (
	"errors"
	"fmt"
	"github.com/couchbase/gocb"
	"hoods.com/mapquest/models"
)

type CouchbaseConnection struct {
	*gocb.Bucket
}

func GetCouchbaseConnection(cfg *models.Config) (*CouchbaseConnection, error) {

	cluster, err := gocb.Connect("couchbase://" + cfg.DBHost)
	if err != nil {
		msg := fmt.Sprintf("CouchbaseConnection :: Couchbase :: Cluster :: Error : %s", err.Error())
		fmt.Println(msg)
		return nil, errors.New(msg)
	}

	auth := &gocb.ClusterAuthenticator{
		Buckets: gocb.BucketAuthenticatorMap{
			cfg.BucketName: gocb.BucketAuthenticator{
				Password: cfg.BucketPassword,
			},
		},
		Username: cfg.NoSqlUser,
		Password: cfg.NoSqlPassword,
	}

	err = cluster.Authenticate(auth)

	if err != nil {
		msg := fmt.Sprintf("CouchbaseConnection :: Couchbase :: Cluster > Auth :: Error : %s", err.Error())
		fmt.Println(msg)
		return nil, errors.New(msg)
	}

	var bucket *gocb.Bucket

	// It takes some time to connect to couchbase bucket thats why #todo add a timeout
	for {
		bucket, err = cluster.OpenBucket(cfg.BucketName, "")
		if err == nil {
			break
		}
		//msg := fmt.Sprintf("CouchbaseConnection :: Couchbase :: Cluster > Open Bucket :: Error : %s :: trying again ...", err.Error())
		//fmt.Println(msg)
	}

	fmt.Println("\n\nConnected to NOSQL DB")

	return &CouchbaseConnection{bucket}, nil
}
